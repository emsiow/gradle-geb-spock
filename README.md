gradle-geb-spock
---
Spike project for functional test suite

- Gradle
- Geb
- Spock

Run
---

`$ ./gradlew clean chromeTest -Denv=google`
